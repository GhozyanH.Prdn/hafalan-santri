<?php 
class C_pendaftaran_tpq extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->helper('url');
 
	}
 
	public function index(){
		$this->load->view('pendaftaran_tpq');
	
	}


	public function aksi_upload(){
		$config['upload_path']          = './assets/images/pendaftaran_tpq/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 10000;
		$config['max_width']            = 5000;
		$config['max_height']           = 5000;
 
		$this->load->library('upload', $config);
 
		if ( ! $this->upload->do_upload())
		{
			$this->load->view('V_error');
			
		}
		else{
			$img = $this->upload->data();
			$foto = $img['file_name'];
			$nama = $this->input->post('nama');
			$jenjang = $this->input->post('jenjang');
			$sekolah = $this->input->post('sekolah');
			$email = $this->input->post('email');
			$ttl = $this->input->post('ttl');
			$alamat = $this->input->post('alamat');
			$telp = $this->input->post('telp');
			$jk = $this->input->post('jk');

			$data = array(
				'nama' => $nama,
				'jenjang' => $jenjang,
				'sekolah' => $sekolah,
				'email' => $email,
				'ttl' => $ttl,
				'alamat' => $alamat,
				'telp' => $telp,
				'jk' => $jk,
				'foto' => $foto,
								);
			$this->db->insert('pendaftaran_tpq',$data);
			$this->load->view('awal');
		}
 }
}