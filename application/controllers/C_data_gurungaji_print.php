<?php 
class C_data_gurungaji_print extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('M_gurungaji');
		$this->load->helper('url');
	}
	public function index(){
		$data['gurungaji'] = $this->M_gurungaji->get_user_all();
        $this->load->view('data_gurungaji_print', $data);
	}
 
}