<?php 
class C_data_hafalan_ustadz extends CI_Controller{
	function __construct(){
		parent::__construct();		
		$this->load->model('M_hafalan_ustadz');
		$this->load->helper('url');
	}
	public function index(){
		$data['data_hafalan_ustadz'] = $this->M_hafalan_ustadz->get_user_all();
        $this->load->view('data_hafalan_ustadz',$data);
	}
 
}