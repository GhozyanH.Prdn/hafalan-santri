<html>
<head>
	<title>DATABASE DATA SANTRI</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/santri.css'); ?>">
	<link rel="icon" href="<?php echo base_url('assets/images/sippekat.png'); ?>">
</head>
<body>
	<div id="header">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<div class="login">
			<p class="login1">SANTRI</p>
		</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>
	<div id="daftar">
		<h1 class="daftarmuz">DATABASE DATA SANTRI</h1>
	</div>
		<table>
			<tr>
			<th>No</th>
			<th>NAMA SANTRI</th>
			<th>JENJANG PENDIDIKAN</th>
			<th>ASAL SEKOLAH</th>
			<th>EMAIL</th>
			<th>TANGGAL LAHIR</th>
			<th>ALAMAT</th>
			<th>NO TELEPON</th>
			<th>JENIS KELAMIN</th>
			<th>PHOTO SANTRI</th>
			<th colspan="3">PILIHAN AKSI</th>
		</tr>
		<?php 
		$no = 1;
		foreach($santri as $u){ 
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $u->nama ?></td>
			<td><?php echo $u->jenjang ?></td>
			<td><?php echo $u->sekolah ?></td>
			<td><?php echo $u->email ?></td>
			<td><?php echo $u->ttl ?></td>
			<td><?php echo $u->alamat ?></td>
			<td><?php echo $u->telp ?></td>
			<td><?php echo $u->jk ?></td>
			<td><img src="<?php echo base_url(); ?>./assets/images/pendaftaran_tpq/<?=$u->foto;?>"' width='150' height='100'></td> 
			<td><a href='form_ubah.php?nis=".$data['nis']. onClick="return confirm('Apakah Anda Yakin?')">Lihat</a></td>
			<td><a href='form_ubah.php?nis=".$data['nis']. onClick="return confirm('Apakah Anda Yakin?')">Ubah</a></td>
			<td><a href='proses_hapus.php?nis=".$data['nis']. onClick="return confirm('Apakah Anda Yakin?')">Hapus</a></td>
		<?php } ?>
		</tr>

		</table>
		<form action="C_santri_print" method="Post">
		<button class="btcetak" type="submit" title="PRINT" name="print" onClick="return confirm('Apakah Anda Yakin?')">CETAK</button></form>	
</body>
</html>