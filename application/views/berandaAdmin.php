<html>
<head>
	<title>Beranda-ADMIN</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/berandaAdmin.css'); ?>">
	<link rel="icon" href="<?php echo base_url('assets/images/sippekat.png'); ?>">
</head>
<body>
	<div id="header">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<div class="login">
			<p class="login1">SANTRI</p>
		</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>
	<div class="logout">
		<a href="<?php echo base_url('index.php/C_loginAdmin'); ?>" class="tulisan1" onClick="return confirm('Apakah Anda Yakin?')">LOGOUT</a>
	</div>
	<div id="santri">
		<a href="<?php echo base_url(); ?>index.php/C_santri" class="tulisan5">DATABASE DATA SANTRI</a>
	</div>
	<div id="wali">
		<a href="<?php echo base_url(); ?>index.php/c_data_walisantri" class="tulisan6">DATABASE DATA WALI SANTRI</a>
	</div>
	<div id="ustadz">
		<a href="<?php echo base_url(); ?>index.php/c_data_gurungaji" class="tulisan7">DATABASE DATA GURU NGAJI</a>
	</div>
	<div id="walibaru">
		<a href="<?php echo base_url(); ?>index.php/C_waliSantri" class="tulisan4">REGISTRASI WALI SANTRI BARU</a>
	</div>
	<div id="ustadzbaru">
		<a href="<?php echo base_url(); ?>index.php/C_guruNgaji/" class="tulisan2">REGISTRASI GURU NGAJI BARU</a>
	</div>
	<div id="hafalanbaru">
		<a href="<?php echo base_url(); ?>index.php/C_hafalan" class="tulisan8">INPUT HAFALAN SURAH</a>
	</div>
	<div id="hafalan">
		<a href="<?php echo base_url(); ?>index.php/C_data_hafalan" class="tulisan3">LAPORAN HAFALAN SANTRI</a>
	</div>
</body>
</html>