<html>
<head><link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/input_hafalan.css'); ?>">
	<title>HAFALAN</title>
		<link rel="icon" href="<?php echo base_url('assets/images/sippekat.png'); ?>">
		<script type="text/javascript" src="<?php echo base_url('assets/js/penjumlahan.js'); ?>"></script>
</head>
<body>
	<div id="header">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<div class="login">
			<p class="login1">SANTRI</p>
		</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>

	<div id="daftar">
	<h1>HAFALAN SURAH</h1>
	</div>

	<div id="tabelmuz">
	<?php echo form_open_multipart('index.php/C_hafalan/aksi_upload');?>
	<table>
	<tr>
		<label for="nama" class="santri">PILIH NAMA SANTRI</label>
		<?php
			echo "<select name='nama' class='inputsantri' required> <option value='' disabled selected>------------------Pilih Nama Santri-----------------</option>";
			foreach ($pendaftaran_tpq->result() as $row) {  
			echo "<option value='".$row->nama."'>".$row->nama."</option>";
			}
			echo "</select>";
		?>
	</tr>
	<tr>
		<label for="ustadz" class="ustadz">PILIH GURU NGAJI</label>
		<?php
			echo "<select name='ustadz' class='inputustadz' required> <option value='' disabled selected>------------------Pilih Guru Ngaji-------------------</option>";
			foreach ($pilih_ustadz->result() as $row) {  
			echo "<option value='".$row->nama."'>".$row->nama."</option>";
			}
			echo "</select>";
		?>
	</tr>
	<tr>
		<label for="surah" class="surah">PILIH SURAH AL-QUR'AN</label>
		<?php
			echo "<select name='surah' class='inputsurah' required> <option value='' disabled selected>------------------Pilih Surah Alqur'an---------------</option>";
			foreach ($surah_alquran->result() as $row) {  
			echo "<option value='".$row->surah."'>".$row->surah."</option>";
			}
			echo "</select>";
		?>
	</tr>
	<tr>
		<label for="ayat" class="ayat">AYAT SURAH</label>
		<input type="text" name="ayat" class="inputayat" required>
	</tr>
	<tr>
		 <label for="tanggal" class="tanggal">TANGGAL VERIFIKASI</label>
  <input type="date" name="tanggal" class="inputtgl" required>  
	</tr>
			<label id="penilaian">PENILAIAN NILAI HAFALAN</label>
			<label for="hafalan" id="lbhafalan">HAFALAN</label>
			<input type="number" name="hafalan" id="inputhafal" min="1" max="60" required>
			<label for="tajwid" id="lbtajwid">TAJWID</label>
			<input type="number" name="tajwid" id="inputtajwid" min="1" max="20" required>
			<label for="makhroj" id="lbmakhroj">MAKHROJ</label>
			<input type="number" name="makhroj" id="inputmakhroj" min="1" max="20" required>
			<label for="hasil_akhir" id="lbhasil">HASIL AKHIR</label>
			<input type="text" name="hasil_akhir" id="inputhasil" readonly>
			<label for="keterangan" id="lbketerangan">KETERANGAN</label>
			<input type="text" name="keterangan" id="inputket" readonly>
			<input type="button" id="bthitung" name="hitung" onclick="penjumlahan()" value="HITUNG">
			<input type="button" id="btreset" name="hitung" onclick="reset()" value="BATAL">	
	<button id="btdaftar" type="submit" value="DAFTAR" onClick="return confirm('Apakah Anda Yakin?')">TAMBAHKAN</button>
	<button id="btbatal" type="reset" value="Batal">BATAL</button>
			<div id="tabelket">
			<p class="ket">KETERANGAN PENILAIAN</p>
			<hr color="white" size="2" class="garis1">
			<p class="ket1">1. Nilai Maksimal Hafalan Adalah 60</p>
			<p class="ket2">2. Nilai Maksimal Tajwid Adalah 20</p>
			<p class="ket3">3. Nilai Maksimal Makhroj Adalah 20</p>
			<p class="ket4">4. Rumus Penilaian Adalah [Hafalan + Tajwid + Makhroj]</p>
			<p class="ket5">5. Hasil Akhir Nilai Lulus Minimal Adalah 70</p>
			</table>
	</form>
	</div>

</body>
</html>