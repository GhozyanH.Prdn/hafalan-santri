<html>
<head>
	<title>SIPPEKAT - Login</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/login.css'); ?>">
	<link rel="icon" href="<?php echo base_url('assets/images/sippekat1.png'); ?>">
</head>
<body>
	<div id="header">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<div class="login">
			<p class="login1">SANTRI</p>
		</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip" style="width: 440px; height: 450px">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>
	<div id="daftar">
		<h1 class="daftarmuz">LOGIN SEBAGAI</h1>
	</div>
	<div id="tabelmuz">
		<div class="admin">
			<a href="<?php echo base_url(); ?>index.php/c_loginadmin" class="labelmuz">ADMIN SIM Santri</a>	
		</div>
		<div class="walisantri">
			<a href="<?php echo base_url(); ?>index.php/c_loginWaliSantri" class="labelmuz" style="top: 100px;">Wali Santri / Orang Tua</a>
		</div>
		<div class="ustadz">
			<a href="<?php echo base_url(); ?>index.php/C_loginUstadz" class="labelmuz">Guru Ngaji / Ustadz</a>
		</div>
	</div>
</body>
</html>