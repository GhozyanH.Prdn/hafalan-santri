<html>
<head>
	<title>HAFALAN</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/hafalan.css'); ?>">
	<link rel="icon" href="<?php echo base_url('assets/images/sippekat.png'); ?>">
</head>
<body>
	<div id="header">
		<div class="sippekat">SIM SANTRI</div>
		<hr color="#EBC64E" size="7" class="garis">
		<div class="sippekat1">Sistem Informasi Hafalan Al-Qur'an Santri</div>
		<div class="login">
			<p class="login1">SANTRI</p>
		</div>
		<img src="<?php echo base_url('assets/images/sippekat.png'); ?>" class="logosip">
		<img src="<?php echo base_url('assets/images/sippekat1.png'); ?>" class="logosip1">
	</div>
	<div id="daftar">
	<h1>DATABASE HAFALAN SANTRI</h1></>
	</div>
		<table>
			<tr>
			<th>No</th>
			<th>NAMA SANTRI</th>
			<th>NAMA GURU NGAJI</th>
			<th>SURAH ALQURAN</th>
			<th>AYAT SURAH</th>
			<th>NILAI HAFALAN</th>
			<th>NILAI TAJWID</th>
			<th>NILAI MAKHROJ</th>
			<th>HASIL AKHIR</th>
			<th>KETERANGAN</th>
			<th>TANGGAL VERIFIKASI</th>

		</tr>
		<?php 
		$no = 1;
		foreach($data_hafalan_walisantri as $u){ 
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $u->nama ?></td>
			<td><?php echo $u->ustadz ?></td>
			<td><?php echo $u->surah ?></td>
			<td><?php echo $u->ayat ?></td>
			<td><?php echo $u->hafalan ?></td>
			<td><?php echo $u->tajwid ?></td>
			<td><?php echo $u->makhroj ?></td>
			<td><?php echo $u->hasil_akhir ?></td>
			<td><?php echo $u->keterangan ?></td>
			<td><?php echo $u->tanggal ?></td>
			<?php } ?>
			</tr>
		</table>
		<form action="cetak-laporan" method="Post">
		<button class="btcetak" type="submit" title="PRINT" name="print" onClick="return confirm('Apakah Anda Yakin?')">CETAK</button></form>
</body>
</html>