function cek(){
	if(document.getElementById('inputhafal').value == "" || document.getElementById('inputtajwid').value == "" || document.getElementById('inputmakhroj').value == ""){
		alert("Isi data");
		exit;
	}
}

function penjumlahan(){
	cek();
	var hafalan = document.getElementById('inputhafal').value;
	var tajwid = document.getElementById('inputtajwid').value;
	var makhroj = document.getElementById('inputmakhroj').value;
	var hasil_akhir = parseInt(hafalan) + parseInt(tajwid) + parseInt(makhroj);
	document.getElementById('inputhasil').value = hasil_akhir;
	if(hasil_akhir>=70) {
		document.getElementById('inputket').value = "Lulus";		
	}
	else {
		document.getElementById('inputket').value = "Mengulang";
	}
}

function reset(){
	document.getElementById('inputhafal').value = "";
	document.getElementById('inputtajwid').value = "";
	document.getElementById('inputmakhroj').value = "";
	document.getElementById('inputhasil').value = "";
	document.getElementById('inputket').value = "";
}

